import torchaudio
import torch
import boto3
import tempfile
import uuid
from naogi import NaogiModel
from fairseq.checkpoint_utils import load_model_ensemble_and_task_from_hf_hub
from fairseq.models.text_to_speech.hub_interface import TTSHubInterface
from pydub import AudioSegment

class Model(NaogiModel):
	def _save(self, wav, rate):
		with tempfile.TemporaryDirectory(dir="/tmp") as _dir:
			with open(_dir+'/wav', 'wb') as file:
				torchaudio.save(
					file,
					wav.cpu().reshape(1, len(wav)),
					rate,
					format='wav'
				)
			pydub_wav = AudioSegment.from_file(_dir+'/wav')
			pydub_wav.export(_dir+'/wav'+'.mp3', format='mp3')
			object_name = str(uuid.uuid4()) + '.mp3'
			client = boto3.client('s3')
			client.upload_file(_dir+'/wav'+'.mp3', 'digup-alpha', object_name)

			return client.generate_presigned_url(
				'get_object',
				Params={
					'Bucket': 'digup-alpha',
					'Key': object_name,
				},
				ExpiresIn=7200
			)

	def load_model(self):
		self.device = torch.device(
			"cuda") if torch.cuda.is_available() else torch.device("cpu")

		models, cfg, self.task = load_model_ensemble_and_task_from_hf_hub(
			"facebook/fastspeech2-en-ljspeech",
			arg_overrides={"vocoder": "hifigan", "fp16": False}
		)
		self.model = models[0].to(self.device)

		TTSHubInterface.update_cfg_with_data_cfg(cfg, self.task.data_cfg)
		self.generator = self.task.build_generator([self.model], cfg)

	def warming_up(self):
		self.load_model()

	def init_model(self):
		self.load_model()

	def prepare(self, params):
		self.prompt = params['prompt']

	def predict(self):
		sample = TTSHubInterface.get_model_input(self.task, self.prompt)

		sample['net_input']['src_tokens'] = sample['net_input']['src_tokens'].to(
			self.device)
		sample['net_input']['src_lengths'] = sample['net_input']['src_lengths'].to(
			self.device)
		sample['speaker'] = sample['speaker'].to(
			self.device) if sample['speaker'] != None else torch.tensor([[0]]).to(self.device)

		wav, rate = TTSHubInterface.get_prediction(
			self.task,
			self.model,
			self.generator,
			sample
		)

		src = self._save(wav, rate)

		return {'src': src}
